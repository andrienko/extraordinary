Building
---

To build sources you have to have node.js, gulp and bower installed.
 
To install node.js - go to http://nodejs.org. To install bower ang gulp - run

    npm install --g gulp bower
 
When you have node installed - run following:

    npm install
    bower install
    gulp
   
The project will be (re)built into **result** folder.

Add some tiles
===

There's a function to add tiles from JSON (for further AJAX or something, idk)

To generate some sample tiles and add them - go like

    extraordinary.generators.nodes.addTiles();
    
In console.

Yep! Get rid of generators.js on production.