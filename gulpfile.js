/*global require*/
var gulp = require('gulp'),
  less = require('gulp-less'),
  autoprefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  sourcemaps = require('gulp-sourcemaps'),
  rimraf = require('gulp-rimraf');

var cleancssPlugin = require("less-plugin-clean-css"),
  cleancss = new cleancssPlugin({
    advanced: true
  });

var src = './';
var dest = './result/';

var assets = 'assets/';

var js = 'js/';
var css = 'css/';
var img = 'img/';
var font = 'font';

var components = src + assets + js + 'components/';

var third_party = [
    'wookmark/jquery.wookmark.min.js',
    'jquery/dist/jquery.min.js',
    'imagesloaded/imagesloaded.pkgd.min.js',
    'disqus-thread/disqus-thread.html'
];


third_party.map(function (a, b) {
  third_party[b] = components + a;
});

gulp.task('default', ['static', 'css', 'js']);

gulp.task('static', function () {

  gulp.src(third_party.concat([
      src + assets + font + '/*',
      src + assets + img + '/**/*'
      ]), {
      base: src + assets
    })
    .pipe(gulp.dest(dest + assets));

  gulp.src(src + '*.html').pipe(gulp.dest(dest));
});

gulp.task('js', function () {


  gulp.src(src + assets + js + '*.js')
    //.pipe(uglify())
    .pipe(gulp.dest(dest + assets + js));
});

var less_files = [
  './assets/less/style.less'
];

gulp.task('css', function () {
  gulp.src(less_files)
    .pipe(sourcemaps.init())
    .pipe(less({
      paths: [src + assets + js],
      plugins: [cleancss]
    }))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dest + assets + css));

});

gulp.task('rm', function () {
  gulp.src(dest, {
      read: false
    })
    .pipe(rimraf());
});
