/*global jQuery,window,document*/

var extraordinary = extraordinary || {};

jQuery(function () {

  (function (parent, $) {
    // Settings for Wookmark plugin (http://www.wookmark.com/jquery-plugin)
    parent.wookmark_settings = {
      align: "center",
      container: $('#tiles'),
      itemWidth: 470,
      offset: 30,
      outerOffset: 30
    };

    parent.app = {

      init: function () {
        $(window).resize(function (e) {
          parent.app.arrange_tiles();
        });

        parent.wookmark_settings.container.imagesLoaded().progress(function () {
          parent.app.arrange_tiles();
        });

        this.init_share_button();
        this.init_sidebar_show_button();

        parent.app.arrange_tiles();
      },

      init_sidebar_show_button: function () {
        var sidebarVisible = false;
        $('#sidebar-show')
          .addClass('phone-visible')
          .click(function () {
            sidebarVisible = !sidebarVisible;
            $(this)
              .html((sidebarVisible ? 'Hide' : 'Show') + ' menu')
              .prev()
              .toggleClass('shown', sidebarVisible);

          })
          .html('Show menu')
          .prev()
          .addClass('hidden');
      },

      init_share_button: function () {

        $('#button-share')
          .show()
          .click(function (e) {

            var $this = $(this);
            var $share = $('#share_diag');

            $this.toggleClass('pressed');

            $share.toggle($this.hasClass('pressed'));

            var sharepos_left = $this.offset().left - $share.outerWidth() + $this.outerWidth();

            $share.css({
              left: sharepos_left,
              top: $('#header').outerHeight()
            });

            e.stopPropagation();
          });

        $(document).click(function () {
          $('#share_diag').hide();
          $('#button-share').removeClass('pressed');
        });
      },

      arrange_tiles: function () {

        var width = $(window).outerWidth();
        if (width <= 1440) parent.wookmark_settings.itemWidth = 300;
        else parent.wookmark_settings.itemWidth = 470;

        $('.tile')
          .width(parent.wookmark_settings.itemWidth)
          .css('margin', 0)
          .wookmark(parent.wookmark_settings);
      },

      add_tiles: function (tiles_array) {

        var added_tiles = [];

        for (var index in tiles_array) {
          var added_tile = this.add_single_tile(tiles_array[index]);
          added_tiles.push(added_tile);
        }

        return added_tiles;
      },

      add_single_tile: function (tile_object) {

        var tile = this.build_tile_from_object(tile_object);


        var tilesContainer = parent.wookmark_settings.container;

        $(tilesContainer).append(tile);

        tile.imagesLoaded().progress(function (a) {
          $(a.elements[0]).removeClass('unloaded');
          parent.app.arrange_tiles();
        });

        return tile[0];
      },

      build_tile_from_object: function (tile) {

        var catClass = " cat-more";
        if (tile.category) catClass = " cat-" + tile.category;
        var tileHTML = '<div class="unloaded tile' + catClass + '">';
        if (tile.link) tileHTML += '<a href="' + tile.link + '">';
        if (tile.image) tileHTML += '<div class="image"><img src="' + tile.image + '" alt="' + tile.title + '"/></div>';

        if (tile.text || tile.title) {

          tileHTML += '<div class="text">';

          tileHTML += '<div class="meta">';

          if (tile.date) {
            var monthsNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            tile.date = new Date(tile.date);
            var dateString = tile.date.getDate() + ' ' + monthsNames[tile.date.getMonth()] + ' ' + tile.date.getFullYear();
            tileHTML += '<div class="date">' + dateString + '</div>';
          }

          if (tile.comments)
            tileHTML += '<div class="comments">' + tile.comments + ' comments</div>';

          tileHTML += '</div>';

          if (tile.title) tileHTML += '<h4>' + tile.title + '</h4>';
          if (tile.text) tileHTML += '<p>' + tile.text + '</p>';
          tileHTML += '</div>';
        }
        if (tile.link) tileHTML += '</a>';
        tileHTML += '</div>';
        return $(tileHTML);
      }
    };

  })(extraordinary, jQuery);
  extraordinary.app.init();
});
